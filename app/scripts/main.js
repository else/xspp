/*jshint unused: vars */
requirejs.config({
  paths: {
    jquery: '../bower_components/jquery/dist/jquery',
    bootstrap: '../bower_components/bootstrap/dist/js/bootstrap',
    'angular-scenario': '../bower_components/angular-scenario/angular-scenario',
    'angular-sanitize': '../bower_components/angular-sanitize/angular-sanitize',
    'angular-route': '../bower_components/angular-route/angular-route',
    'angular-resource': '../bower_components/angular-resource/angular-resource',
    'angular-mocks': '../bower_components/angular-mocks/angular-mocks',
    'angular-cookies': '../bower_components/angular-cookies/angular-cookies',
    angular: '../bower_components/angular/angular'
  },
  shim: {
    angular: {
      exports: 'angular'
    },
    'angular-route': [
      'angular'
    ],
    'angular-cookies': [
      'angular'
    ],
    'angular-sanitize': [
      'angular'
    ],
    'angular-resource': [
      'angular'
    ],
    'angular-mocks': {
      deps: [
        'angular'
      ],
      exports: 'angular.mock'
    }
  },
  priority: [
    'angular'
  ]
});

//http://code.angularjs.org/1.2.1/docs/guide/bootstrap#overview_deferred-bootstrap
window.name = 'NG_DEFER_BOOTSTRAP!';

requirejs([
  'angular',
  'app',
  'angular-route',
  'angular-cookies',
  'angular-sanitize',
  'angular-resource',
  'jquery'
], function(angular, app, ngRoutes, ngCookies, ngSanitize, ngResource, $) {
  'use strict';
  /* jshint ignore:start */
  var $html = angular.element(document.getElementsByTagName('html')[0]);
  /* jshint ignore:end */

  /* The following snippet is covered undered the The MIT License (MIT).
   * 
   * Copyright (c) 2014 Anatoly Pashin
   * 
   * Permission is hereby granted, free of charge, to any person obtaining a copy of
   * this software and associated documentation files (the "Software"), to deal in
   * the Software without restriction, including without limitation the rights to
   * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
   * the Software, and to permit persons to whom the Software is furnished to do so,
   * subject to the following conditions:
   * 
   * The above copyright notice and this permission notice shall be included in all
   * copies or substantial portions of the Software.
   * 
   * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
   * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
   * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
   */
  (function() {
    function Menu(cutLabel, copyLabel, pasteLabel) {
      var gui = require('nw.gui');
      var menu = new gui.Menu();
      var cut = new gui.MenuItem({
        label: cutLabel || 'Cut',
        click: function() {
          document.execCommand('cut');
          console.log('Menu:', 'cutted to clipboard');
        }
      });
  
      var copy = new gui.MenuItem({
        label: copyLabel || 'Copy',
        click: function() {
          document.execCommand('copy');
          console.log('Menu:', 'copied to clipboard');
        }
      });
  
      var paste = new gui.MenuItem({
        label: pasteLabel || 'Paste',
        click: function() {
          document.execCommand('paste');
          console.log('Menu:', 'pasted to textarea');
        }
      });
  
      menu.append(cut);
      menu.append(copy);
      menu.append(paste);
  
      return menu;
    }
  
    var menu = new Menu(/* pass cut, copy, paste labels if you need i18n*/);
    $(document).on('contextmenu', function(e) {
      e.preventDefault();
      menu.popup(e.originalEvent.x, e.originalEvent.y);
    });
  })();

  angular.element().ready(function() {
    angular.resumeBootstrap([app.name]);
  });
});
